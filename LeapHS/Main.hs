import Control.Applicative
import Control.Monad (when)
import Data.Aeson
import Data.IORef
import qualified Data.ByteString.Lazy as BS
import GHC.Float
import Graphics.UI.SDL            as FX
import Graphics.UI.SDL.Image      as FX
import Graphics.UI.SDL.Types      as FX
import Graphics.UI.SDL.Rotozoomer as FX
import System.Console.ANSI

import Audio
import Leap
import Leap.Hand as Hand
import Leap.Gesture
import Leap.Pointable hiding (length)
import Leap.BaseTypes (Vec3, Mat3)

-- * Instruments
type Instruments = [Instrument]

data Instrument = Instrument
  { instrumentArea   :: Vec3 -> Bool
  , instrumentHeight :: Vec3 -> Bool
  , instrumentAudio  :: (FilePath, Int)
  , instrumentDown   :: Bool
  , instrumentUp     :: Bool
  }

instrumentShouldOn :: Instrument -> LeapHand -> Bool
instrumentShouldOn instrument hand = inArea && inHeight
  where inArea   = instrumentArea instrument $ palmPosition hand
        inHeight = instrumentHeight instrument $ palmPosition hand

-- ** Interaction with instruments

-- | Play instrument if it should be playd
playInstrumentIfDown :: Instrument -> IO ()
playInstrumentIfDown instrument =
  when (instrumentDown instrument) $
    (uncurry playFile) (instrumentAudio instrument)

-- | Update instruments using hand positions
updateInstruments :: [LeapHand] -> Instruments -> Instruments
updateInstruments = map . updateInstrument

updateInstrument :: [LeapHand] -> Instrument -> Instrument
updateInstrument hands instrument = 
  instrument { instrumentDown = beforeUp && nowDown
             , instrumentUp   = not nowDown
             }
 where beforeUp = instrumentUp instrument 
       nowDown  = any (instrumentShouldOn instrument) hands

-- * Specific instruments
drums = -- [ bass, sideStick ]
        [ tomtom, snare, cymbal ]

bass :: Instrument
bass = Instrument
 { instrumentArea   = \(x,y,z) -> (abs x > 50) && z < 0
 , instrumentHeight = \(x,y,z) -> y < 130
 , instrumentAudio  = ("data/drum_bass.wav", 300)
 , instrumentDown   = False
 , instrumentUp     = True
 }

tomtom :: Instrument
tomtom = Instrument
 { instrumentArea   = \(x,y,z) -> x > 10
 , instrumentHeight = \(x,y,z) -> y < 130
 , instrumentAudio  = ("data/tomtom_low.wav", 300)
 , instrumentDown   = False
 , instrumentUp     = True
 }

sideStick :: Instrument
sideStick = Instrument
 { instrumentArea   = \(x,y,z) -> x < (-50) -- && z > 10
 , instrumentHeight = \(x,y,z) -> y < 100
 , instrumentAudio  = ("data/sound2.wav", 400)
 , instrumentDown   = False
 , instrumentUp     = True
 }

snare :: Instrument
snare = Instrument
 { instrumentArea   = \(x,y,z) -> x < (-10) && z > 10
 , instrumentHeight = \(x,y,z) -> y < 100
 , instrumentAudio  = ("data/drum_snare2.wav", 400)
 , instrumentDown   = False
 , instrumentUp     = True
 }

cymbal :: Instrument
cymbal = Instrument
 { instrumentArea   = \(x,y,z) -> x < (-10) && z < (-10)
 , instrumentHeight = \(x,y,z) -> y < 150
 , instrumentAudio  = ("data/cymbal1.wav", 1100)
 , instrumentDown   = False
 , instrumentUp     = True
 }

-- * Main program
main :: IO ()
main = do
 -- Video visualiser: initialisation
 -- bg    <- load "data/drumsS.png"
 -- stick <- load "data/drumstick.png"
 -- FX.init [InitVideo]
 -- FX.setVideoMode 1024 700 32 []
 -- canvas <- FX.getVideoSurface
 -- FX.blitSurface bg Nothing canvas (Just (Rect 0 0 1024 700))

 -- Audio visualiser: initialisation
 initAudio
 drumRef <- newIORef drums

 -- Processing
 runLeap defaultConfig $ \x -> do
   -- Update drumps
   newDrums <- modifyReadIORef drumRef (updateInstruments (hands x))

   -- Audio visualiser: consumption
   mapM_ playInstrumentIfDown newDrums

   -- Debug visualiser: consumption
   -- clearScreen
   -- mapM_ print $ hands x
   print x
   -- mapM_ (print .  palmPosition) $ hands x

   -- Video visualisers: consumption
   --    FX.blitSurface bg Nothing canvas (Just (Rect 0 0 1024 700))
   --    mapM_ (\h -> do let (a,b,c)    = Hand.direction h
   --                        (p1,p2,p3) = Hand.palmPosition h
   --                        f2d        = float2Double
   --                    drawDrumstick stick canvas (adjustPos (p1, p2)) ((*90) $ f2d a)
   --          ) (hands x)
   --    FX.flip canvas
   -- where adjustPos (x,y) = (round (1024*(-x)/200 + 512), round (700*y/200 + 350))

-- -- * Visualisation
--
-- -- | Aux operation to draw a drumstick.
-- -- TODO: This operation is way too slow
-- drawDrumstick stick canvas (x,y) angle = do
--   rotatedSurface <- FX.rotozoom stick angle 0.5 True 
--   let (w,h) = (surfaceGetWidth rotatedSurface, surfaceGetHeight rotatedSurface)
--   FX.blitSurface rotatedSurface Nothing canvas (Just (Rect x y w h))

-- * Auxiliary code
fst3 :: (a,b,c) -> a
fst3 (a,b,c) = a

snd3 :: (a,b,c) -> b
snd3 (a,b,c) = b

-- * IORef: modify and return the new value
modifyReadIORef :: IORef a -> (a -> a) -> IO a
modifyReadIORef ref f = do
  modifyIORef ref f
  readIORef ref

-- Unused
-- data Bar = Bar
--  { barYaw   :: Double 
--  , barPitch :: Double 
--  , barRot   :: Double 
--  }

-- drawBar stick canvas bar = do
--   drawDrumstick stick canvas (125, 125) (barYaw   bar)
--   drawDrumstick stick canvas (125, 425) (barPitch bar)
--   drawDrumstick stick canvas (725, 125) (barRot   bar)
