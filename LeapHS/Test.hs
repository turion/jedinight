module Main where

import Graphics.UI.SDL            as FX
import Graphics.UI.SDL.Types      as FX
import Graphics.UI.SDL.Rotozoomer as FX

data Bar = Bar
 { barYaw   :: Double 
 , barPitch :: Double 
 , barRot   :: Double 
 }

drawBar canvas bar = do
  drawBarWithAngle canvas (125, 125) (barYaw   bar)
  drawBarWithAngle canvas (125, 425) (barPitch bar)
  drawBarWithAngle canvas (725, 125) (barRot   bar)

drawBarWithAngle canvas (x,y) angle = do
  surfaceR <- createRGBSurface [SWSurface] 200 20 32 0xFF000000 0x00FF0000 0x0000FF00 0x000000FF
  FX.fillRect surfaceR (Just (Rect 0 0 200 20)) (Pixel 0xFFFFFF)
  rotatedSurface <- FX.rotozoom surfaceR angle 1 True 
  let (w,h) = (surfaceGetWidth rotatedSurface, surfaceGetHeight rotatedSurface)
  FX.blitSurface rotatedSurface Nothing canvas (Just (Rect x y w h))

main :: IO ()
main = do
     FX.init [InitVideo]
     FX.setVideoMode 1024 700 32 []
     canvas <- FX.getVideoSurface
     drawBar canvas (Bar 45 45 45)
     FX.flip canvas
     eventLoop
     FX.quit
 where
     eventLoop = FX.waitEventBlocking >>= checkEvent
     checkEvent (KeyUp _) = return ()
     checkEvent _         = eventLoop
