## LeapMotion websocket client in Haskell

I have a leap motion. I want to have a bit of a play with it. I also was curious about investigating the current state of doing some practical things in Haskell, so I thought I'd combine these two interests.

This code is experimental and written almost entirely for my own edification. It is not well written both from a code quality and an architectural point of view. It is very unlikely to ever resemble anything like "production ready" and you shouldn't trust it as far as you can throw it. 

If you ignore that warning, on your own head be it, but feel free to do whatever you like with the code.
