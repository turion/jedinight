import YampaCore.FRPWithMonad hiding (main)
import Control.Arrow
import Wiimote.GetAccData
import System.CWiid
import LeapHS.Audio
import Graphics.UI.SDL            as FX
import Graphics.UI.SDL.Image      as FX
import Graphics.UI.SDL.Types      as FX
import Graphics.UI.SDL.Rotozoomer as FX
import OpenGLAnimation.Lightsaber
import Control.Concurrent
import Data.IORef
import Graphics.UI.GLUT

main = mainGL
-- main = mainSDL

mainGL = do
  initAudio
  wm <- initWiimote
  angle <- newIORef (0.0 :: GLfloat)
  forkIO $ glThread angle
  reactimate $ (liftSF $ cwiidGetAcc wm)
       >>> arr (listToTuple . unCWiidAcc)
       >>> arr (\(x,y,z) -> (x - 116, y - 94, z - 120))
       >>> arr toAction
       >>> (arr id &&& setAngle angle) >>> arr fst
       >>> (arr id) &&& (arr (maybe False ((> margin).intensity)) >>> edge)
       >>> arr (\(x,e) -> if e then x else Nothing)
       >>> liftSFop (\x -> playAction x) --  >> print x)
 where margin = 60

mainSDL = do
    FX.init [InitVideo]
    FX.setVideoMode 1024 700 32 []

    -- Sound
    initAudio
    wm <- initWiimote
    bg    <- load "LeapHS/data/drumsS.png"
    stick <- load "LeapHS/data/drumstick.png"
    lastShow <- newIORef Nothing

    canvas <- FX.getVideoSurface
    FX.blitSurface bg Nothing canvas (Just (Rect 0 0 1024 700))

    reactimate $ (liftSF $ cwiidGetAcc wm)
                  >>> arr (listToTuple . unCWiidAcc)
                  >>> arr (\(x,y,z) -> (x - 116, y - 94, z - 120))
                  >>> arr (id &&& (toAction >>> fmap intensity >>> (maybe False (> margin))))
                  >>> (arr toAction *** edge)
                  >>> arr (\(x,e) -> if e then x else Nothing)
                  >>> liftSFop (\x -> do
                        playAction x
                        print x
                        drawDrums canvas bg stick x lastShow
                        return ()
                      )
 where margin = 60

showBass canvas bg stick = do
  rotatedSurface <- FX.rotozoom stick (90) 0.5 True 
  let (w,h) = (surfaceGetWidth rotatedSurface, surfaceGetHeight rotatedSurface)
  FX.blitSurface rotatedSurface Nothing canvas (Just (Rect 200 300 w h))
  return ()

showSnare canvas bg stick = do
  rotatedSurface <- FX.rotozoom stick 0 0.5 True 
  let (w,h) = (surfaceGetWidth rotatedSurface, surfaceGetHeight rotatedSurface)
  FX.blitSurface rotatedSurface Nothing canvas (Just (Rect 300 200 w h))
  return ()

showCymbal canvas bg stick = do
  rotatedSurface <- FX.rotozoom stick (45) 0.5 True 
  let (w,h) = (surfaceGetWidth rotatedSurface, surfaceGetHeight rotatedSurface)
  FX.blitSurface rotatedSurface Nothing canvas (Just (Rect 110 110 w h))
  return ()

drawDrums canvas bg stick x lastShow = do
  FX.blitSurface bg Nothing canvas (Just (Rect 0 0 1024 700))
  case x of
    Nothing         -> do last <- readIORef lastShow
                          case last of
                            Nothing -> return ()
                            Just (_,  x) | x == 0 -> writeIORef lastShow Nothing
                            Just (b@(Bass _),  x) -> do showBass   canvas bg stick
                                                        writeIORef lastShow (Just (b, x-1))
                            Just (b@(Snare _), x) -> do showSnare  canvas bg stick
                                                        writeIORef lastShow (Just (b, x-1))
                            Just (b@(Cymbal _), x) -> do showCymbal canvas bg stick
                                                         writeIORef lastShow (Just (b, x-1))
    Just (Bass _)   -> do showBass canvas bg stick
                          writeIORef lastShow (fmap (\y -> (y, 90)) x)
    Just (Snare _)  -> do showSnare canvas bg stick 
                          writeIORef lastShow (fmap (\y -> (y, 90)) x)
    Just (Cymbal _) -> do showCymbal canvas bg stick 
                          writeIORef lastShow (fmap (\y -> (y, 90)) x)
  FX.flip canvas

playAction :: Maybe Action -> IO ()
playAction Nothing           = return ()
playAction (Just (Bass   _)) = playFile "LeapHS/data/drum_bass.wav" 300
playAction (Just (Snare  _)) = playFile "LeapHS/data/drum_snare2.wav" 400
playAction (Just (Cymbal _)) = playFile "LeapHS/data/cymbal1.wav" 1100

-- * Drum actions
data Action = Bass Int | Snare Int | Cymbal Int
 deriving (Show)

intensity :: Action -> Int
intensity (Bass e) = e
intensity (Snare e) = e
intensity (Cymbal e) = e

toAction :: (Int, Int, Int) -> Maybe Action
toAction (x,y,z)
  | y > marginY && y > 2 * abs x = Just $ Cymbal y
  | x < (-marginX)               = Just $ Bass (abs x)
  | x > marginX                  = Just $ Snare x
  | otherwise                    = Nothing
 where marginY = 25
       marginX = 20

calcAngle :: Maybe Action -> GLfloat
calcAngle Nothing = 0
calcAngle (Just (Bass x)) = fromIntegral x
calcAngle (Just (Snare x)) = fromIntegral x
calcAngle (Just (Cymbal x)) = fromIntegral x

setAngle angle = liftSFop $ (writeIORef angle) . (/2) . calcAngle

-- * Auxiliary functions
listToTuple :: [Int] -> (Int, Int, Int)
listToTuple [x,y,z] = (x,y,z)

