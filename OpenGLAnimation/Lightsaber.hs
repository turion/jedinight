module OpenGLAnimation.Lightsaber where

import Graphics.UI.GLUT
import Control.Monad
import Data.IORef
glThread :: IORef GLfloat -> IO ()
glThread angle = do
    (_progName, _args) <- getArgsAndInitialize
    _window <- createWindow "Hello World"
    sizeref <- newIORef $ Size 300 300
    reshapeCallback $= (Just $ reshape sizeref)
    displayCallback $= display sizeref angle
    idleCallback $= Just (display sizeref angle)
    mainLoop
    forever $ return ()

reshape :: IORef Size -> ReshapeCallback
reshape sizeref size = do
    writeIORef sizeref size

idle :: IORef GLfloat -> IdleCallback
idle angle = do
  angle $~! (+ 1)
  postRedisplay Nothing

points :: [ Vertex3 GLfloat ]
points = [ Vertex3 1 1 0, Vertex3 (-1) 1 0, Vertex3 (-1) (-1) 0, Vertex3 1 (-1) 0]
square = renderPrimitive Quads $ mapM_ vertex points
longslab = preservingMatrix $ do
    translate $ Vector3 0 0 (0.2 :: GLfloat)
    scale 0.2 10 (0.1 :: GLfloat)
    square

rod :: Vector3 GLfloat -> GLfloat -> GLfloat -> IO()
rod position theta phi = preservingMatrix $ do
    translate position
    rotate phi $ Vector3 0 1 (fromIntegral 0)
    rotate theta $ Vector3 1 0 (fromIntegral 0)
    replicateM_ 4 $ do
        rotate (90 :: GLfloat) $ Vector3 0 1 (fromIntegral 0)
        longslab

eyelength = 5 :: GLfloat
scene phi = preservingMatrix $ do
        translate $ Vector3 0 0 (-50 :: GLfloat)
        rod (Vector3 0 0 (-5)) (phi*3.2) phi

display :: IORef Size -> IORef GLfloat -> DisplayCallback
display sizeref angle = do
    print =<< readIORef angle
    matrixMode $= Projection
    loadIdentity
    perspective 45 1 1 1000
    matrixMode $= Modelview 0
    loadIdentity
    clear [ColorBuffer]
    Size width height <- readIORef sizeref
    phi <- readIORef angle
    viewport $= (Position 0 0, Size (width `div` 2) height)
    preservingMatrix $ do
        translate $ Vector3 eyelength 0 0
        scene phi
    viewport $= (Position (width `div` 2) 0, Size (width `div` 2) height)
    preservingMatrix $ do
        translate $ Vector3 (-eyelength) 0 0
        scene phi
    swapBuffers
